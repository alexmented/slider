const debounce = (func, wait, immediate) => {
    let timeout, args, context, timestamp, result;
    if (null == wait) wait = 100;
  
    function later() {
      let last = Date.now() - timestamp;
  
      if (last < wait && last >= 0) {
        timeout = setTimeout(later, wait - last);
      } else {
        timeout = null;
        if (!immediate) {
          result = func.apply(context, args);
          context = args = null;
        }
      }
    }
  
    let debounced = function() {
      context = this;
      args = arguments;
      timestamp = Date.now();
      let callNow = immediate && !timeout;
      if (!timeout) timeout = setTimeout(later, wait);
      if (callNow) {
        result = func.apply(context, args);
        context = args = null;
      }
  
      return result;
    };
  
    debounced.clear = function() {
      if (timeout) {
        clearTimeout(timeout);
        timeout = null;
      }
    };
  
    debounced.flush = function() {
      if (timeout) {
        result = func.apply(context, args);
        context = args = null;
  
        clearTimeout(timeout);
        timeout = null;
      }
    };
  
    return debounced;
  };
  

//slider


const previousElement = document.querySelector('.previousElement');
const nextElement = document.querySelector('.nextElement');
const app = document.querySelector('.app');
const elementsNumber = 4;

const nextElementSlide = () => {
    if (document.querySelector('.addAnimation')) {
        document.querySelector('.addAnimation').classList.remove('addAnimation');
     }
    const active = document.querySelector('.active');
    active.classList.remove('active');
    for (let i = 0; i < app.children.length; i++) {
        const el = app.children[i];
        const index = parseInt(el.dataset.index);

        // Скрываем уходящий
        if (index === 3) {
            el.classList.add('hide');
        }
        else {
            el.classList.remove('hide');
        }

        // Меняем положение элементов
        if (index !== elementsNumber) {
            const oldX = el.style.transform.replace(/[^\-?\d.]/g, '');
            el.style.transform = `translateX(${+oldX + 30}vw)`;
            el.setAttribute('data-index', index + 1); 
        }
        else {
            el.style.transform = `translateX(${i * -30}vw)`;
            el.setAttribute('data-index', index - (elementsNumber - 1)); 
            el.classList.add('addAnimation');
        }

    }
    const newActive = document.querySelector('div[data-index="2"]');
    newActive.classList.add('active');
};

const previousElementSlide = () => {
    if (document.querySelector('.addAnimation')) {
        document.querySelector('.addAnimation').classList.remove('addAnimation');
    }
    const active = document.querySelector('.active');
    active.classList.remove('active');
    for (let i = app.children.length - 1; i > -1; i--) {
        const el = app.children[i];
        const index = parseInt(el.dataset.index);

        // Скрываем уходящий
        if (index === 1) {
            el.classList.add('hide');
        }
        else {
            el.classList.remove('hide');
        }


        // Меняем положение
        if (index !== 1) {
            const oldX = el.style.transform.replace(/[^\-?\d.]/g, '');
            el.style.transform = `translateX(${+oldX - 30}vw)`;
            el.setAttribute('data-index', index - 1);
        }
        else {
            el.style.transform = `translateX(${((elementsNumber - 1) - i) * 30}vw)`;
            el.setAttribute('data-index', index + (elementsNumber - 1)); 
        }
    
    }
    const newActive = document.querySelector('div[data-index="2"]');
    newActive.classList.add('active');
};

// drag n droo


if (window.innerWidth <= 768) {
nextElement.hidden = true;
previousElement.hidden = true;
app.touchstart = function(event) {

    let shiftX = event.clientX - app.getBoundingClientRect().left;
    let shiftY = event.clientY - app.getBoundingClientRect().top;
  
    app.style.position = 'absolute';
    app.style.zIndex = 1000;

    moveAt(event.pageX);
  
    function moveAt(pageX) {
      app.style.left = pageX - shiftX + 'px';
    }
  
    function onMouseMove(event) {
            debounce(moveAt(event.pageX), 100);
    }
  
    document.addEventListener('touchmove', onMouseMove);
  
    app.touchend = function() {
        if (app.getBoundingClientRect().left > -1) {
            moveAt(shiftX);
        }
        if (app.children[3].getBoundingClientRect().right + window.innerWidth / 10 < window.innerWidth) {
            moveAt(-app.children[3].getBoundingClientRect().width + shiftX - window.innerHeight / 5);
        }
      document.removeEventListener('touchmove', onMouseMove);
      app.touchend = null;
    };
  
  };
  
  app.ondragstart = function() {
    return false;
  };
}

// listeners
nextElement.addEventListener('click',() => {
    if (window.innerWidth > 768) {
        nextElementSlide();
    }
});

previousElement.addEventListener('click',() => {
    if (window.innerWidth > 768) {
        previousElementSlide();
    }
});
